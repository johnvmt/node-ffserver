module.exports = function(config) {
	return new Ffserver(config);
};

var path = require('path');
var fs = require('fs');
var childProcess = require('child_process');
var Utils = require('./Utils');
var configDefaults = require('./defaults');

function Ffserver(config) {
	this.config = {};
	if(typeof config.complete === 'function')
		this.config.complete = config.complete;
	if(typeof config.started === 'function')
		this.config.started = config.started;
	var serverConfig = this.getServerConfig(config.server);
	var feedsConfig = this.getFeedsConfig(config.feeds);
	var streamsConfig = this.getStreamsConfig(config.feeds);
	this.start(serverConfig, feedsConfig, streamsConfig);
}

Ffserver.prototype.getServerConfig = function(server) {
	return Utils.objectMerge(configDefaults.server, server);
};

Ffserver.prototype.getStreamsConfig = function(feeds) {
	var streamsComplete = {};
	var self = this;
	Utils.objectForEach(feeds, function(feed, key) {
		streamsComplete = Utils.objectMerge(streamsComplete, self.getFeedStreamsConfig(feed, key));
	});
	return streamsComplete;
};

Ffserver.prototype.getFeedStreamsConfig = function(feed, defaultKey) {
	var self = this;
	// TODO merge??
	var streamsComplete = Utils.objectMerge(feed.streams, {});
	if(Object.keys(streamsComplete).length == 0) // add default stream
		streamsComplete[defaultKey] = {};

	Utils.objectForEach(streamsComplete, function(stream, streamKey) {
		streamsComplete[streamKey] = self.getStreamConfig(stream, streamKey)
	});

	return streamsComplete;
};

Ffserver.prototype.getFeedsConfig = function(feeds) {
	var feedsComplete = {};
	var self = this;
	Utils.objectForEach(feeds, function(feed, key) {
		feedsComplete[key] = self.getFeedConfig(feed, key);
	});
	return feedsComplete;
};

Ffserver.prototype.getFeedConfig = function(feed, key) {
	// TODO deep merge
	var self = this;
	var feedComplete = Utils.objectMerge(configDefaults.feed, feed);
	delete feedComplete.streams;
	return feedComplete;
};

Ffserver.prototype.getStreamConfig = function(stream, feedKey) {
	// TODO deep merge
	return streamComplete = Utils.objectMerge({Feed: feedKey + ".ffm"}, configDefaults.stream, stream);
};

Ffserver.prototype.start = function(server, feeds, streams) {
	var confPath = path.join(configDefaults.confDir, Utils.uniqueId() + '.conf');
	this.writeConfigFile(confPath, server, feeds, streams);

	var self = this;

	var ffserverArgs = '-f ' + confPath;
	var ffserverProcess = childProcess.spawn(configDefaults.bins.ffserver, ffserverArgs.split(' '));

	ffserverProcess.stderr.on('data', function(data) {
		//console.log("STDERR OUTPUT: ", data.toString());
	});

	ffserverProcess.on('exit', function (exitCode) {
		try {
			fs.unlinkSync(confPath); // delete config file
		}
		catch(error) {}

		if(typeof self.config.complete === "function") {
			if(exitCode == 0)
				self.config.complete(null, true);
			else
				self.config.complete(exitCode, null);
		}

		// stop() function was called
		if(typeof self.stopCallback === 'function') {
			self.stopCallback(null, true);
			delete self.stopCallback;
		}
	});

	self.ffserverProcess = ffserverProcess;

};

Ffserver.prototype.writeConfigFile = function(path, server, feeds, streams) {
	var configStr = this.configFileString(server, feeds, streams);
	fs.writeFileSync(path, configStr);
};

Ffserver.prototype.configFileString = function(server, feeds, streams) {
	// generate a .conf file to a string
	var conf = '';

	conf += sectionString(server);

	Utils.objectForEach(feeds, function(feed, key) {
		conf += '<Feed ' + key + '.ffm>\n'
			+ sectionString(feed)
			+ '</Feed>\n';
	});

	Utils.objectForEach(streams, function(stream, key) {
		conf += '<Stream ' + key + '>\n'
			+ sectionString(stream)
			+ '</Stream>\n';
	});

	return conf;

	function sectionString(section) {
		var str = '';
		Utils.objectForEach(section, function(value, key) {
			str += valueString(value, key);
		});
		return str;
	}

	function valueString(value, key) {
		if(typeof value === 'string')
			return key + ' ' + value + '\n';
		else if(typeof value === 'number')
			return key + ' ' + String(value) + '\n';
		else if(value instanceof Array)
			return key + ' ' + value.join(' ') + '\n';
		else if(typeof value === 'object' && value != null) {
			var conf = '';
			Utils.objectForEach(value, function(innerValue, innerKey) {
				conf += valueString(innerValue, key + ' ' + innerKey);
			});
			return conf;
		}
		else // null
			return key + '\n';
	}
};

Ffserver.prototype.stop = function(callback) {
	if(typeof callback === 'function')
		this.stopCallback = callback;
	this.ffserverProcess.kill();
};
