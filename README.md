# ffserver-wrapper #
Basic ffserver wrapper, with ffserver binary for darwin (OSX) and CentOS

### How to use: Basic ###

	var ffserver = require('ffserver-wrapper');
	
	// creates ffserver port on port 8000, using default options
    // includes one 1280x720 webm stream at http://<your-ip>:8000/live
    	
	var ffs = ffserver({
    	server: {HTTPPort: 8000},
    	feeds: {live: {}},
    	complete: function() {
    		console.log("Server died");
    	}
    });
    
    setTimeout(function() {
    	ffs.stop(function() {
    		console.log("Server shutdown complete")
    	})
    }, 10000);

### How to use: Slightly more advanced ###

	var ffserver = require('ffserver-wrapper');

	var ffs = ffserver({
		server: {
			HTTPPort: 8000
		},
		feeds: {
			live: {
				ACL: {
					allow: '127.0.0.1' // allow broadcasts from the local machine only
				},
				streams: {
					live: {
						VideoSize: '1280x720', // override video size
						AudioChannels: 1
					}
				}
			}
		}
	});