var path = require('path');

var config = {bins: {}};

config.bins.ffserver = path.join(__dirname, 'bin', process.platform, 'ffserver');

config.confDir = path.join(__dirname, 'ffserverConfs');

config.server = {
	HTTPPort: 8000,
	HTTPBindAddress: '0.0.0.0',
	MaxHTTPConnections: 2000,
	MaxClients: 1000,
	MaxBandwidth: 100000,
	CustomLog: '-'
};

config.feed =  {
	FileMaxSize: '50M',
	ACL: {
		allow: '0.0.0.0 255.255.255.255'
	}
};

config.stream =  {
	Format: 'webm',
	VideoFrameRate: 30,
	VideoSize: '1280x720',
	/*	AudioChannels: 1,
	AudioCodec: 'libvorbis',
	AudioSampleRate: 48000,
	AVOptionAudio: {
		flags: '+global_header'
	}, */
	MaxTime: 0,
	AVOptionVideo: {
		me_range: 16,
		qdiff: 4,
		qmin: 4,
		qmax: 40,
		flags: '+global_header'
	},
	PreRoll: 0,
	StartSendOnKey: null
/*	Metadata: {
		author: "\"author\"",
		copyright: "\"copyright\"",
		title: "\"Web app name\"",
		comment: "\"comment\""
	} */
};

module.exports = config;